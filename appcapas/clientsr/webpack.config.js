const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
    entry: './src/app/index.js',
    output: {
        path: __dirname + '/public/js',
        filename: 'bundle.js'
    },
     //sección
     module: {
        rules: [{
            test: /\.vue$/,
            use: 'vue-loader'
        }]
    },
    plugins: [
        new VueLoaderPlugin()
    ]
    //sección
    /*module: {
        rules: [{
            test: /\.vue$/,
            use: 'vue-loader'
        }]
    },
    plugins: [
        new VueLoaderPlugin()
    ]*/
}