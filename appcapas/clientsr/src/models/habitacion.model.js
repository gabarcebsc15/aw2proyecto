const mongoose = require('mongoose');
const {Schema} = mongoose; 

const HabitacionSchema = new Schema(
    {
        titulo : {type: String, required : true},
        subtitulo : {type: String, required : true},
        detalle : {type: String, required : true},
        tarifa : {type: String, required : true}
    }
);

module.exports = mongoose.model("Habitacion", HabitacionSchema);
