const mongoose = require('mongoose');
const {Schema} = mongoose; 

const ReservaSchema = new Schema(
    {
        usuario : {type: String, required : true},
        habitacion : {type: String, required : true},
        fecha : {type: String, required : true},
        
    }
);

module.exports = mongoose.model("Reserva", ReservaSchema);
