module.exports = {
    Usuario : require('./usuario.model'),
    Habitacion : require('./habitacion.model'),
    Reserva : require('./reserva.model')
};