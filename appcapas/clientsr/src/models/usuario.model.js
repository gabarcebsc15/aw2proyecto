const mongoose = require('mongoose');
const {Schema} = mongoose; 

const UsuarioSchema = new Schema(
    {
        firstname : {type: String, required : true},
        lastname : {type: String, required : true},
        username: {type: String, required : true},
        password: {type: String, required : true}
    }
);
UsuarioSchema.pre("save", async function(next){
  const User = this;
  if(!User.isModified("password"))
  {
      next();
  }
  User.password = "passwordAplicandoBcrypt";  


})

module.exports = mongoose.model("Usuario", UsuarioSchema);
