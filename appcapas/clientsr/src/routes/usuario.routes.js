const {Router} = require('express')

module.exports = function({UsuarioController}){
    const router = Router(); //crear un router
    //llamar los diferentes verbos usados en el controlador
    router.get('/:userId',  UsuarioController.get ); //consultar un usuario por ID
    router.get('/' , UsuarioController.getAll ); //consultar todos los usuarios
    router.post('/',  UsuarioController.create ); //recibe body para crear un nuevo usuario
    router.patch('/:userId',  UsuarioController.update ); // recibe body para actualizar segUn ID un usuario
    router.delete('/:userId',  UsuarioController.delete ); // recibe ID para eliminar un usuario
    return router;

}