const {Router} = require('express')

module.exports = function({ReservaController}){
    const router = Router(); //crear un router
    //llamar los diferentes verbos usados en el controlador
    router.get('/:reservaId',  ReservaController.get ); //consultar un usuario por ID
    router.get('/' , ReservaController.getAll ); //consultar todos los usuarios
    router.post('/',  ReservaController.create ); //recibe body para crear un nuevo usuario
    router.patch('/:reservaId',  ReservaController.update ); // recibe body para actualizar segUn ID un usuario
    router.delete('/:reservaId',  ReservaController.delete ); // recibe ID para eliminar un usuario
    return router;

}