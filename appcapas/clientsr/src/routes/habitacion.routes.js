const {Router} = require('express')

module.exports = function({HabitacionController}){
    const router = Router(); 
    router.get('/:habitacionId',  HabitacionController.get ); 
    router.get('/' , HabitacionController.getAll ); 
    router.post('/',  HabitacionController.create ); 
    router.patch('/:habitacionId',  HabitacionController.update ); 
    router.delete('/:habitacionId',  HabitacionController.delete ); 
    return router;

}