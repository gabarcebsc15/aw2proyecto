const BaseRepository = require('./base.repository');

let _habitacion = null;
class HabitacionRepository extends BaseRepository
{
    constructor ({Habitacion})
    {
        super(Habitacion);
        _habitacion = Habitacion;
    }
    async getUserByTitulo(titulo)
    {
        return await _habitacion.findOne({titulo})
    }
}

module.exports = HabitacionRepository;