module.exports = {
    UsuarioRepository: require('./usuario.repository'),
    HabitacionRepository: require('./habitacion.repository'),
    ReservaRepository: require('./reserva.repository')

}
