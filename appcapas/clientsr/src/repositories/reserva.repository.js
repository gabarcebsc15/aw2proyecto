const BaseRepository = require('./base.repository')

let _reserva = null;
class ReservaRepository extends BaseRepository
{
    constructor ({Reserva})
    {
        super(Reserva);
        _reserva = Reserva;
    }
    async getUserByUsuario(usuario)
    {
        return await _reserva.findOne({usuario})
    }
}

module.exports = ReservaRepository;