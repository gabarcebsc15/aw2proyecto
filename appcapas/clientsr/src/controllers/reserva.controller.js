let _reservaService = null
class ReservaController
{
    constructor({ReservaService})
    {
        _reservaService = ReservaService;

    }
    //get es una consulta indivdual
    //localhost : 5000/usuario/25
    //{nombre: john , Username : cualquiera, pass : nose}
    async get (req, res)
    {
       const {reservaId /*, userName*/} = req.params;
       const reserva = await _reservaService.get(reservaId);
       return res.send(reserva);

    }
    //getAll
    async getAll(req, res)
    {
        const reservas = await _reservaService.getAll();
        return res.send(reservas);
    }
    //create
    async create(req, res)
    {
        const {body} =  req;
        console.log(body)
        
        const  createdReserva = await _reservaService.create(body);
        res.send(createdReserva);

    }
    //update
    async update(req, res)
    {
        const {reservaId } =  req.params;
        const {body} =  req;
        const updatedReserva =  await _reservaService.update(reservaId, body );
        res.send(updatedReserva);
    }
    //delete 
    async delete(req, res)
    {
        const {reservaId } =  req.params;
        const deletedReserva = await _reservaService.delete(reservaId);
        return res.send(deletedReserva);
    }
}
module.exports = ReservaController;