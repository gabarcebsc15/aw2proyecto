
let _usuarioService = null
class UsuarioController
{
    constructor({UsuarioService})
    {
        _usuarioService = UsuarioService;

    }
    //get es una consulta indivdual
    //localhost : 5000/usuario/25
    //{nombre: john , Username : cualquiera, pass : nose}
    async get (req, res)
    {
       const {userId /*, userName*/} = req.params;
       const user = await _usuarioService.get(userId);
       return res.send(user);

    }
    //getAll
    async getAll(req, res)
    {
        const users = await _usuarioService.getAll();
        return res.send(users);
    }
    //create
    async create(req, res)
    {
        const {body} =  req;
        console.log(body)
        
        const  createdUser = await _usuarioService.create(body);
        res.send(createdUser);

    }
    //update
    async update(req, res)
    {
        const {userId } =  req.params;
        const {body} =  req;
        const updatedUser =  await _usuarioService.update(userId, body );
        res.send(updatedUser);
    }
    //delete 
    async delete(req, res)
    {
        const {userId } =  req.params;
        const deletedUser = await _usuarioService.delete(userId);
        return res.send(deletedUser);
    }
}
module.exports = UsuarioController;