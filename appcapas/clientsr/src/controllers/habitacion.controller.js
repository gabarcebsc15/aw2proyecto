let _habitacionService = null
class HabitacionController
{
    constructor({HabitacionService})
    {
        _habitacionService = HabitacionService;

    }
    //get es una consulta indivdual
    //localhost : 5000/usuario/25
    //{nombre: john , Username : cualquiera, pass : nose}
    async get (req, res)
    {
       const {habitacionId /*, userName*/} = req.params;
       const habit = await _habitacionService.get(habitacionId);
       return res.send(habit);

    }
    //getAll
    async getAll(req, res)
    {
        const habits = await _habitacionService.getAll();
        return res.send(habits);
    }
    //create
    async create(req, res)
    {
        const {body} =  req;
        console.log(body)
        
        const  createdHabitacion = await _habitacionService.create(body);
        res.send(createdHabitacion);

    }
    //update
    async update(req, res)
    {
        const {habitacionId } =  req.params;
        const {body} =  req;
        const updatedHabitacion =  await _habitacionService.update(habitacionId, body );
        res.send(updatedHabitacion);
    }
    //delete 
    async delete(req, res)
    {
        const {habitacionId } =  req.params;
        const deletedHabitacion = await _habitacionService.delete(habitacionId);
        return res.send(deletedHabitacion);
    }
}
module.exports = HabitacionController;