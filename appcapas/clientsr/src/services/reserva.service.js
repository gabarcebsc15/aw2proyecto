const BaseService = require('./base.services');
let _reservaRepository = null;
class ReservaService extends BaseService
{
    constructor({ReservaRepository})
    {
        super(ReservaRepository);
        _reservaRepository= ReservaRepository;

    }
    async getUserByUsuario(usuario)
    {
        return await _reservaRepository.findOne({usuario})
    }

}
module.exports = ReservaService