const BaseService = require('./base.services');
let _userRepository = null;
class UsuarioService extends BaseService
{
    constructor({UsuarioRepository})
    {
        super(UsuarioRepository);
        _userRepository= UsuarioRepository;

    }
    async getUserByUserName(username)
    {
        return await _userRepository.getUserByUsername(username);
    }

}
module.exports = UsuarioService