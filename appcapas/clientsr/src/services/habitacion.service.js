const BaseService = require('./base.services');
let _habitacionRepository = null;
class HabitacionService extends BaseService
{
    constructor({HabitacionRepository})
    {
        super(HabitacionRepository);
        _habitacionRepository= HabitacionRepository;

    }
    async getUserByHabitacionTitle(titulo)
    {
        return await _habitacionRepository.getUserByHabitacionTitle(titulo);
    }

}
module.exports = HabitacionService