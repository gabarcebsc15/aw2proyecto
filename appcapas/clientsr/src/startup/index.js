const express = require('express');
//const { resolve } = require('./container');
let _config = null;
let _express = null;


class Server
{
    constructor({config , router})
    {
        _config = config;
        _express = express().use(router);
    }
    start()
    {
        return new Promise( resolve =>{
            _express.listen(_config.PORT, ()=>{
                console.log(`${_config.APLICATION_NAME} ESTA CORRIENDO POR EL PUERTO ${_config.PORT}`)
            })
            resolve()
        })
    }
}
module.exports = Server;