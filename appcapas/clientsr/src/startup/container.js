const {createContainer, asClass, asFunction, asValue}= require('awilix');
//rqeurir el archivo de configuracion
const config= require('../config');
//startup
const app = require('.')

//models
const {Usuario} = require('../models');
const {Habitacion} = require('../models');
const {Reserva} = require('../models');


//repository
const {UsuarioRepository} = require('../repositories');
const {HabitacionRepository} = require('../repositories');
const {ReservaRepository} = require('../repositories');

//services
const {HabitacionService}= require('../services');
const {UsuarioService}= require('../services');
const {ReservaService}= require('../services');


//controllers
const {HabitacionController} = require('../controllers');
const {UsuarioController} = require('../controllers');
const {ReservaController} = require('../controllers');

//routes
const {HabitacionRoutes} = require('../routes/index.route');
const {UsuarioRoutes} = require('../routes/index.route');
const {ReservaRoutes} = require('../routes/index.route');
const Routes = require('../routes')



const container=createContainer();

container.register(
    {
        app: asClass(app).singleton(),
        router: asFunction(Routes).singleton(),
        config: asValue(config)
         
    }
).register(
    {
        HabitacionService: asClass(HabitacionService).singleton(),
        UsuarioService: asClass(UsuarioService).singleton(),
        ReservaService: asClass(ReservaService).singleton()


    }


).register(
    {
        HabitacionController: asClass(HabitacionController.bind(HabitacionController)).singleton(),
        UsuarioController: asClass(UsuarioController.bind(UsuarioController)).singleton(),
        ReservaController: asClass(ReservaController.bind(ReservaController)).singleton()
    }

).register(
    {
        HabitacionRoutes: asFunction(HabitacionRoutes).singleton(),
        UsuarioRoutes: asFunction(UsuarioRoutes).singleton(),
        ReservaRoutes: asFunction(ReservaRoutes).singleton()
    }


).register(
    {
        Usuario: asValue(Usuario),
        Habitacion: asValue(Habitacion),
        Reserva: asValue(Reserva),

    }

).register(
    {
        UsuarioRepository: asClass(UsuarioRepository).singleton(),
        HabitacionRepository: asClass(HabitacionRepository).singleton(),
        ReservaRepository: asClass(ReservaRepository).singleton()
    }
)

module.exports= container;















/*const { createContainer, asClass, asFunction, asValue } = require('awilix');

// config
const config = require('../config');
//startup
const app =  require('.')

//models

const { User } = require('../models')

//repositorys

const { UserRepository } = require('../repositories')


//services

const { HomeService } = require('../services');
const { UserService } = require('../services');


//controllers

const { HomeController } = require('../controllers');
const { UserController } = require('../controllers');

//routes

const { HomeRoutes} = require('../routes/index.routes');
const { UserRoutes} = require('../routes/index.routes');
const Routes =  require('../routes')




const container = createContainer();


container.register(
    {
        app: asClass(app).singleton(),
        router: asFunction(Routes).singleton(),
        config: asValue(config)
    }
).register(
    {
        HomeService: asClass(HomeService).singleton(),
        UserService: asClass(UserService).singleton()
    }
).register(
    {
        HomeController: asClass(HomeController.bind(HomeController)).singleton(),
        UserController: asClass(UserController.bind(UserController)).singleton()
    }
).register(
    {
        HomeRoutes: asFunction(HomeRoutes).singleton(),
        UserRoutes: asFunction(UserRoutes).singleton()
    }
).register(
    {
        User: asValue(User)
    }
).register(
    {
        UserRepository: asClass(UserRepository).singleton()
    }
)



module.exports = container;
 */























































